<!doctype html>
<html lang="en">

<head>
  <title>Index Customer</title>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS v5.2.1 -->
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">

</head>

<body>
  <header>
    <!-- place navbar here -->
  </header>
  <main>
    <a href="{{ route('customers.create') }}" class="btn btn-primary">Create a Customer</a>
    
    <h2>List users</h2>

    @include('customers.search')

    {{-- show message --}}
    @if(Session::has('success'))
        <p class="text-success">{{ Session::get('success') }}</p>
    @endif

    {{-- show error message --}}
    @if(Session::has('error'))
        <p class="text-danger">{{ Session::get('error') }}</p>
    @endif
        <div class="table-responsive">
          @if (!empty($customers))
          <table class="table table-striped
          table-hover	
          table-borderless
          table-primary
          align-middle">
              <thead class="table-light">
                  <tr>
                      <th>#</th>
                      <th>Name</th>
                      <th>Email</th>
                      <th>Address</th>
                      <th>Tel</th>
                      <th>Age</th>
                      <th>Gender</th>
                      <th colspan="2">Action</th>
                  </tr>
                  </thead>
                  @foreach ($customers as $customer)
                    <tbody class="table-group-divider">
                      <tr class="table-primary" >
                        <td scope="row">{{$customer['id']}}</td>
                        <td>{{$customer['name']}}</td>
                        <td>{{$customer['email']}}</td>
                        <td>{{$customer['address']}}</td>
                        <td>{{$customer['tel']}}</td>
                        <td>{{$customer['age']}}</td>
                        <td>{{$customer['gender']}}</td>
                        <td><a href="{{ route('customers.edit', ['id' => $customer['id']]) }}">Edit</a></td>
                        <td>
                          <form action="{{ route('customers.destroy', ['id' => $customer['id']]) }}" method="post">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger btn-common" onclick="return confirm('Confirm delete ?')"><i class="fas fa-trash-alt"></i>Delete</button>
                          </form>
                        </td>
                      </tr>
                    </tbody>
                  @endforeach
          </table>
          {{ $customers->appends(request()->input())->links() }}
          @endif
        </div>
        
  </main>
  <footer>
    <!-- place footer here -->
  </footer>
  <!-- Bootstrap JavaScript Libraries -->
  <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js"
    integrity="sha384-oBqDVmMz9ATKxIep9tiCxS/Z9fNfEXiDAYTujMAeBAsjFuCZSmKbSSUnQlmh/jp3" crossorigin="anonymous">
  </script>

  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.min.js"
    integrity="sha384-7VPbUDkoPSGFnVtYi0QogXtr74QeVeeIs99Qfg5YCF+TidwNdjvaKZX19NZ/e6oz" crossorigin="anonymous">
  </script>
</body>

</html>