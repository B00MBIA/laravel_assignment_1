<form action="{{ route('customers.index') }}" method="get">
    <div class="mb-2">
        <label for="">Email/Name</label>
        <input type="text" name="search_email_name" value="{{old('search_email_name', request()->get('search_email_name'))}}" class="form-control">
    </div>
    <div class="mb-2">
        <label for="">Address</label>
        <input type="text" name="search_address" value="{{old('search_address', request()->get('search_address'))}}" class="form-control">
    </div>
    <div class="mb-2">
        <label for="">Phone</label>
        <input type="text" name="search_phone" value="{{old('search_phone', request()->get('search_phone'))}}" class="form-control">
    </div>
    <div class="mb-2">
        <label for="">From Age</label>
        <input type="number" name="search_from_age" value="{{old('search_from_age', request()->get('search_from_age'))}}" class="form-control">
    </div>
    <div class="mb-2">
        <label for="">To Age</label>
        <input type="number" name="search_to_age" value="{{old('search_to_age', request()->get('search_to_age'))}}" class="form-control">
    </div>
    <div class="mb-2">
        <label for="">Gender</label>
        <select name="search_gender" id="" class="form-control">
            <option value="">all</option>
            <option value="0" {{old('search_gender', request()->get('search_gender')) == '0' ? 'selected' : ''}}>male</option>
            <option value="1" {{old('search_gender', request()->get('search_gender')) == '1' ? 'selected' : ''}}>female</option>
        </select>
    </div>
    <div class="mb-2">
        <button type="submit" class="btn btn-primary">Search</button>
    </div>
</form>