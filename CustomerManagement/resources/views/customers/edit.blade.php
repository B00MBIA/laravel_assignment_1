<!doctype html>
<html lang="en">

<head>
  <title>Edit Customer</title>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS v5.2.1 -->
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">

</head>

<body>
  <header>
    <!-- place navbar here -->
  </header>
  <main>
    <h1>Edit a Customer</h1>
    <form action="{{ route('customers.update', ['id' => $customer->id]) }}" method="post" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <div class="form-group mb-2">
            <label for="">Customer Name</label>
            <input type="text" value="{{ old('name', $customer->name) }}" class="form-control" name="name">
            @error('name')
                <div class="text-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group mb-2">
            <label for="">Customer Email</label>
            <input type="email" value="{{ old('email', $customer->email) }}" class="form-control" name="email">
            @error('email')
                <div class="text-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group mb-2">
            <label for="">Customer Address</label>
            <input type="text" value="{{ old('address', $customer->address) }}" class="form-control" name="address">
            @error('address')
                <div class="text-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group mb-2">
            <label for="">Customer Tel</label>
            <input type="tel" value="{{ old('tel', $customer->tel) }}" class="form-control" name="tel">
            @error('tel')
                <div class="text-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group mb-2">
            <label for="">Customer Birthday</label>
            <input type="date" value="{{ old('birthday', $customer->birthday) }}" class="form-control" name="birthday">
            @error('birthday')
                <div class="text-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group mb-2">
            <label for="">Customer Gender</label>
            <select name="gender" id="" class="form-control">
                <option value="0" {{old('gender', $customer->gender) == 0 ? 'selected' : ''}}>male</option>
                <option value="1" {{old('gender', $customer->gender) == 1 ? 'selected' : ''}}>female</option>
            </select>
            @error('gender')
                <div class="text-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group mb-2">
            <a href="{{ route('customers.index') }}" class="btn btn-secondary">User List</a>
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </form>
  </main>
  <footer>
    <!-- place footer here -->
  </footer>
  <!-- Bootstrap JavaScript Libraries -->
  <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js"
    integrity="sha384-oBqDVmMz9ATKxIep9tiCxS/Z9fNfEXiDAYTujMAeBAsjFuCZSmKbSSUnQlmh/jp3" crossorigin="anonymous">
  </script>

  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.min.js"
    integrity="sha384-7VPbUDkoPSGFnVtYi0QogXtr74QeVeeIs99Qfg5YCF+TidwNdjvaKZX19NZ/e6oz" crossorigin="anonymous">
  </script>
</body>

</html>