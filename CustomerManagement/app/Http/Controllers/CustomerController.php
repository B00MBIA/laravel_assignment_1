<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreCustomerRequest;
use App\Http\Requests\UpdateCustomerRequest;
use App\Models\Customer;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $data = [];
        $customers = Customer::orderBy('id', 'asc');
        // Xử lý tìm kiếm
        if (request()->has('search_email_name')) {
            $email_name = request()->get('search_email_name');
            $customers->where(function($query) use ($email_name) {
                $query->where('name', 'LIKE', "%$email_name%")
                      ->orWhere('email', 'LIKE', "%$email_name%");
            });
        }

        if (request()->has('search_address')) {
            $address = request()->get('search_address');
            $customers->where('address', 'LIKE', "%$address%");
        }

        if (request()->has('search_phone')) {
            $phone = request()->get('search_phone');
            $customers->where('tel', 'LIKE', "%$phone%");
        }

        if (request()->has('search_from_age')) {
            $from_age = request()->get('search_from_age');
            $customers->whereYear('birthday', '<', now()->subYears($from_age)->format('Y'));
        }

        if (request()->has('search_to_age')) {
            $to_age = request()->get('search_to_age');
            $customers->whereYear('birthday', '>=', now()->subYears($to_age +1)->format('Y'));
        }

        if (request()->has('search_gender')) {
            $gender = request()->get('search_gender');
            $customers->where('gender', $gender === '0' ? 0 : 1);
        }
        
        $customers = $customers->paginate(10);
        $data['customers'] = $customers;
        return view('customers.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $data = [];
        return view('customers.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreCustomerRequest $request)
    {
        $dataSave = [
            'name' => $request->name,
            'email' => $request->email,
            'address' => $request->address,
            'tel' => $request->tel,
            'birthday' => $request->birthday,
            'gender' => $request->gender,
        ];
        try {
            Customer::create($dataSave);
            Log::info('Create a Customer successfull.');
            return redirect()->route('customers.index')->with('success', 'Create a Customer successfull.');
        } catch (Exception $exception) {
            Log::error($exception->getMessage());
            return redirect()->route('customers.index')->with('error', 'Create a Customer failture.');
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $data = [];
        $customer = Customer::findOrFail($id);
        $data['customer'] = $customer;

        return view('customers.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateCustomerRequest $request, string $id)
    {
        $customer = Customer::findOrFail($id);
        $dataUpdate = [
            'name' => $request->name,
            'email' => $request->email,
            'address' => $request->address,
            'tel' => $request->tel,
            'birthday' => $request->birthday,
            'gender' => $request->gender,
        ];
        try {
            $customer->update($dataUpdate);
            Log::info('Update a Customer successfull.');
            return redirect()->route('customers.index')->with('success', 'Update a Customer successfull.');
        } catch (Exception $exception) {
            Log::error($exception->getMessage());
            return redirect()->route('customers.index')->with('error', 'Update a Customer failture.');
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $customer = Customer::findOrFail($id);
        try {
            $customer->delete();
            Log::info('Delete a Customer successfull.');
            return redirect()->route('customers.index')->with('success', 'Delete a Customer successfull.');
        } catch (Exception $exception) {
            Log::error($exception->getMessage());
            return redirect()->route('customers.index')->with('error', 'Delete a Customer failture.');
        }
    }
}
