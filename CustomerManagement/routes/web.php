<?php

use App\Http\Controllers\CustomerController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//route customer
Route::get('/customers', action: [CustomerController::class, 'index'])->name('customers.index');

Route::get('/customers/create', action: [CustomerController::class, 'create'])->name('customers.create');

Route::post('/customers', [CustomerController::class, 'store'])-> name('customers.store');

Route::get('/customers/{id}/edit', action: [CustomerController::class, 'edit'])->name('customers.edit');

Route::put('/customers/{id}', action: [CustomerController::class, 'update'])->name('customers.update');

Route::delete('/customers/{id}', action: [CustomerController::class, 'destroy'])->name('customers.destroy');